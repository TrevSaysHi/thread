extends KinematicBody2D

var movement_vect = Vector2(0, 0)
var move_speed = 250.0
var dead_zone = .2
var gravity_speed = 1000
var gravity_cap = 500
var health = 3

var jump_amount = 650
var jump_time = 0

var animation_state

var pixel_shift = 46

enum state {
	IDLE,
	WALK,
	JUMP,
	ATTACK,
	DEATH,
	HURT
}

var is_facing_right = true
var can_be_hit = true

signal on_health_change

var current_state
# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect('on_health_change', $Health, '_on_player_health_change')
	$Timer.connect('timeout', self, '_on_timer_time_out')
	$Timer.set_one_shot(true)
	global.player_health = health
	animation_state = $AnimationTree.get("parameters/playback")
	current_state = state.IDLE
	emit_signal('on_health_change', health)

	if get_parent().name == 'KitchenLevel':
		$Camera2D.limit_left = -1680
		$Camera2D.limit_bottom = 680
		$Camera2D.limit_right = 3017
		$Camera2D.limit_top = -1521
	if get_parent().name == 'CabinentLevel':
		$Camera2D.limit_left = -2025
		$Camera2D.limit_bottom = 560
		$Camera2D.limit_right = 2029
		$Camera2D.limit_top = -10000000
	pass # Replace with function body.

func _process(_delta):
	if health == 0:
		current_state = state.DEATH
		animation_state.travel('idle')
		$Restart.visible = true
		movement_vect.x = 0
		if Input.is_action_pressed('start'):
			if get_parent().name == 'KitchenLevel':
				get_tree().reload_current_scene()
			if get_parent().name == 'CabinentLevel':
				health = 3
				global.player_health = health
				emit_signal('on_health_change', health)
				get_tree().change_scene("res://Level/Level.tscn")
				pass

	# if current_state == state.WIN:
		# deal with this later
	# gets input
	if current_state != state.DEATH:
		if Input.is_action_pressed('ui_right'):
			movement_vect.x = move_speed
		elif Input.is_action_pressed('ui_left'):
			movement_vect.x = -move_speed
		elif Input.get_connected_joypads().size() > 0:
			var x_axis = Input.get_joy_axis(0, JOY_AXIS_0)
			if abs(x_axis) > dead_zone:	
				movement_vect.x = move_speed * x_axis
			else:
				movement_vect.x = 0
		else:
			movement_vect.x = 0
		# turns the player the right direction
		if movement_vect.x > 0 && !is_facing_right:
			$Sprite.set_scale(Vector2(.1, .1))
			$Sprite.position = Vector2($Sprite.position.x + pixel_shift, $Sprite.position.y)
			is_facing_right = true
		elif movement_vect.x < 0 && is_facing_right:
			$Sprite.set_scale(Vector2(-.1, .1))
			$Sprite.position = Vector2($Sprite.position.x - pixel_shift, $Sprite.position.y)
			is_facing_right = false
			
		# Sets correct animation
		if is_on_floor():
			if movement_vect.x != 0:
				animation_state.travel('walk')
				current_state = state.WALK
			else:
				animation_state.travel('idle')
				current_state = state.IDLE
		else: 
			if current_state != state.ATTACK || current_state == state.HURT:
				animation_state.travel('jump')
				current_state = state.JUMP
			else:
				if is_facing_right:
					animation_state.travel('attack')
				else:
					animation_state.travel('attack_l')

		if Input.is_action_just_pressed('attack'):
			$SwipeSound.play()
			current_state = state.ATTACK
			if is_facing_right:
				animation_state.travel('attack')
			else:
				animation_state.travel('attack_l')

		# Jumping logic
		if Input.is_action_just_pressed('jump') && is_on_floor():
			$JumpSound.play()
			jump_time = 0
			movement_vect.y = -jump_amount
		if Input.is_action_pressed('jump'):
			jump_time += _delta
		if Input.is_action_just_released('jump'):
			if movement_vect.y < -100:
				movement_vect.y = -jump_amount * jump_time
	
	# Apply gravity with limits
	if movement_vect.y < gravity_cap:
		movement_vect.y += gravity_speed * _delta

func _physics_process(_delta):
	move_and_slide(movement_vect, Vector2(0, -1))

func attack_finished():
	if is_on_floor():
		current_state = state.IDLE
		animation_state.travel('idle')
	else:
		current_state = state.JUMP
		animation_state.travel('jump')

func _on_PlayerCollision_area_entered(area_2d):
	if area_2d.name == 'EnemyCollision' || area_2d.name == 'SinkDeath':
		if health != 0 && can_be_hit:
			current_state = state.HURT
			animation_state.travel('hurt')
			$Timer.start(.5)
			can_be_hit = false
			health -= 1
			global.player_health = health
			emit_signal('on_health_change', health)
		if area_2d.name == 'SinkDeath':
			self.transform.origin = Vector2(854, -611)
	elif area_2d.name == 'LevelExit':
		get_tree().change_scene("res://CabinentLevel/CabinentLevel.tscn")
	elif area_2d.name == 'Cheese':
		health = 0

func _on_timer_time_out():
	can_be_hit = true