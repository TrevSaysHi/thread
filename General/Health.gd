extends Control

var heart = preload('res://General/Heart.tscn')

func _on_player_health_change(current_health):
	for i in range(0, get_child_count()):
		get_child(i).queue_free()
		# self.remove_child(current_children[i])
	for i in current_health:
		var heart_instance = heart.instance()
		heart_instance.set_position(Vector2(i * 65, self.get_position().y))
		self.add_child(heart_instance)