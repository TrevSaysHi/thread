extends KinematicBody2D

export var movement_radius = 150
export var movement_speed = 150
var sprite_scale = .25

var start_position
var movement_vect = Vector2(0, 0)

var on_screen = false
var going_right = true
var die = false

func _ready():
	$AnimationPlayer.current_animation = 'move'
	start_position = self.transform.origin
	$VisibilityNotifier2D.connect('screen_entered', self, '_on_visibility_screen_entered')
	$VisibilityNotifier2D.connect('screen_exited', self, '_on_visibility_screen_exited')
	$Sprite.set_scale(Vector2(-sprite_scale, sprite_scale))
	pass

func _process(_delta):
	if die:
		get_parent().remove_child(self)
	if on_screen:
		if going_right:
			if self.transform.origin.x < (start_position.x + movement_radius):
				movement_vect.x = movement_speed 
			else:
				going_right = false
				$Sprite.set_scale(Vector2(sprite_scale, sprite_scale))
		else:
			if self.transform.origin.x > (start_position.x - movement_radius):
				movement_vect.x = -movement_speed 
			else:
				going_right = true
				$Sprite.set_scale(Vector2(-sprite_scale, sprite_scale))

func _physics_process(_delta):
	move_and_slide(movement_vect, Vector2(0, -1))

func _on_visibility_screen_entered():
	on_screen = true

func _on_visibility_screen_exited():
	on_screen = false

func _on_EnemyCollision_area_entered(area_2d):
	if area_2d.name == 'Sword':
		die = true